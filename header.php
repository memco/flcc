<?php
// require('../speedy/libs/php_speedy/php_speedy.php');
define('MAGPIE_OUTPUT_ENCODING', 'UTF-8');
define('MAGPIE_INPUT_ENCODING', 'UTF-8');
require_once('_includes/rss_fetch.inc');
require_once('../getid3/getid3/getid3.php');
require_once('config/database.php');
?>
<!doctype html>
<html class="no-js" lang="">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="verify-v1" content="qELAT5WNqUILQmelfR+cKciFptEEbXjiINVw8Rc3MXs=">
	<meta name="description" content="Charismatic Worship, Warm Atmosphere, Spirit Led Church, Where the broken find healing and restoration of purpose">
	<title>Full Life Christian Center,full life christian center,flcc,FLCC,Simi Valley,Simi Valley Churches,Charismatic,John McKendricks,Pastor John McKendricks,KDAR</title>
	<link rel="stylesheet" href="http://yui.yahooapis.com/2.7.0/build/reset-fonts-grids/reset-fonts-grids.css" type="text/css">
	<link rel="stylesheet" href="_includes/style.css" type="text/css">
	<script type="text/javascript" charset="utf-8" src="//code.jquery.com/jquery-1.11.2.min.js"></script>
	<!-- <script type="text/javascript" charset="utf-8" src="_includes/jquery.dataTables.min.js"></script>-->
	<script type="text/javascript" src="//cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js"></script>
</head>
<body>
	<div id="doc" class="yui-t4">
	<div id="hd">
		<h1><span>Full Life</span><span>Christian Center</span></h1>
		<ul id="navigation">
			<li><a href="index.php">Home</a></li>
			<li><a href="about.php">About</a></li>
			<li><a href="articles.php">Articles</a></li>
			<li><a href="sermons.php">Sermon Archive</a></li>
		</ul>
	</div>
	<div id="bd">
		<div class="yui-b" id="sidebar">
			<?php
				$twitter = fetch_rss('http://twitter.com/statuses/user_timeline/23087467.rss');
				if($twitter) {
					echo '<div id="tweets">';
					echo "<h2>Latest Tweets</h2>\n";
					$items = array_slice($twitter->items, 0, 4);
					foreach ($items as $item) {
						$item['description'] = preg_replace('/FullLifer: /su','',$item['description']);
						print '<div class="tweet"><h3><a href="' . $item['link'] . '" title="Tweet:' . $item['title'] . '">' .$item['description']. '</a></h3><p>(' . date("F j, Y, g:i a",$item['date_timestamp']) . ')</p></div>';
						echo "\n";
					}
					echo "</div>";
					unset($items);
				}
			?>

		<?php
		if ($db = mysqli_connect($MYSQL_SERVER, $MYSQL_USER, $MYSQL_PASS, $MYSQL_DB))
		{
			//If we have correctly selected the database, get the stored info
			$query = 'SELECT * FROM `sermon_index` WHERE hidden=0 ORDER BY date_entered DESC LIMIT 0,5';
			$result = $db->query($query);
			$db->close();

			//Iterate through the results of the query
			echo '<h2>Latest Sermons</h2>';
			echo '<ul>';

			while($row = $result->fetch_assoc())
			{
				$path = $row["path"];
				$title = $row["title"];
				echo ("<li><a href=\"" . $path . "\">" . $title . "</a></li>\n");
			}

			echo '</ul>';
		}
		?>
		</div><!-- Sidebar -->
		<div id="yui-main">
			<div class="yui-b">