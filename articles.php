<?php
	require_once('header.php');
?>

<div class="download">
	<p class="abstract">One primary character in J. R. R. Tolkien’s legendarium  “The Lord of the Rings” is the character Gollum; a despicable, hideous character who looks more like a monster than anything else. Readers are often shocked to learn that this hideous creature was not always in this form. At one time Gollum lived life as his altar ego, "Sméagol," a Hobbit. &hellip;</p>
	<img src="images/icons/document.gif">
	<p class="filename"><a href="Gollum%202.pdf" class="document">Gollum.pdf</a></p>
</div>
<?
	require_once('footer.php');
?>