<?php

	function myTruncate($string, $limit, $break=".", $pad="&hellip;")
	{
	  // return with no change if string is shorter than $limit
	  if(strlen($string) <= $limit) return $string;

	  // is $break present between $limit and the end of the string?
	  if(false !== ($breakpoint = strpos($string, $break, $limit))) {
	    if($breakpoint < strlen($string) - 1) {
	      $string = substr($string, 0, $breakpoint) . $pad;
	    }
	  }

	  return $string;
	}

	define('MAGPIE_OUTPUT_ENCODING', 'UTF-8');
	define('MAGPIE_INPUT_ENCODING', 'UTF-8');
	require_once('_includes/rss_fetch.inc');
	
	$john=fetch_rss('http://web.me.com/jmckend/Main_Blog/Blog/rss.xml');
	$michael=fetch_rss('http://flcc.org/blog/?feed=rss2');

	if($john && $michael)
	{
		$johns_items = array_slice($john->items, 0, 1);
		$michaels_items = array_slice($michael->items, 0, 1);
		if ($johns_items && $michaels_items)
		{
			echo "<pre>";
			var_dump($johns_items);
			echo "</pre>";
			echo "<pre>";
			var_dump($michaels_items);
			echo "</pre>";
			$items = array_merge($johns_items,$michaels_items);
			if ($items)
			{
				foreach ($items as $item)
				{
					// Clean up feed for HTML 4.01 strict
					// Wrap paragraphs in <p> tags and truncate the entry after 1000 characters
					$item['content']['encoded'] = myTruncate( preg_replace('/(.*?)<br[^>]*>/su','<p>$1</p>',$item['content']['encoded']),750,"</p>",'&hellip;</p><p class="more">[<a href="' . $item['link'] . '">Read the rest of this entry</a>]</p>');
					// Remove closing / from image and other such tags
					$item['content']['encoded'] = preg_replace('/\/>/','>',$item['content']['encoded']);
					// Add alt text
					$item['content']['encoded'] = preg_replace('/(img.*?(?!alt).*?)>/su','$1 alt="">',$item['content']['encoded']);

					// Print the blog title
					echo '<div><h3><a href="' . $item['link'] . '" title="Blog:' . $item['title'] . '">' .$item['title']. '</a></h3>';
					// Print the date
					echo '<p class="date">(' . date("F j, Y, g:i a",$item['date_timestamp']) . ')</p>';
					// Print the content of the entry
					echo $item['content']['encoded'];
					echo '</div>';
				}
			}
		}
	}

	// echo "<br><h1>John</h1><pre>";
	// echo "<pre>";
	// var_dump($john);
	// echo "</pre>";
	// 
	// echo "<br><h1>Michael</h1><pre>";
	// var_dump($michael);
	// echo "</pre>";
	
?>