<?php
	$title = "Full Life Christian Center: About Us";
	$page_desc = "Find out more about Full Life Christian Center and our mission to reach the lost bruiseed broken and bored";
	require_once ('header.php');
?>
<h2>About Us</h2>
<p>
Our mission statement has undergone many iterations, but over the years our goal of reaching the lost, restoring the bruised broken and bored to the cause of Christ that they might be restored to the fullness of life God intends and become reproducers of the Kingdom of God has become a vision born out of the growth of our hearts.  Our original mission was to "<strong>Regrow</strong> lost limbs.  To <strong>reenact</strong> the civil war that we might <strong>reinvent</strong> history.  To <strong>repeatedly</strong> use alliteration so that people will flee in terror."  However, we decided that this was a rather silly proposition and it has not been mentioned since, but we felt it an important enough part of our history to share here so that people have a sense of how we got here.
</p>

<p>
Full Life began as a very small group of people meeting in a home on Sunday evenings (no, not that kind of home).  Over the years we have grown, and have moved from the home to a dance studio to an Adventist church, then to a storefront, and finally to our current home at Crossroads.  We have always had something of a reputation for being a bit different and that could be attributed to the fact that the 70s were not kind to John.  Though it might be that we are a bunch of cynics who are called to reach out to the cynics.  Might be a little bit of column "A" and a little bit of column "B."  
</p>

<p>
To give a flavor of life in our church it might be helpful first to note that our favorite theologian is Joe Dirt.  On a rare occasion, we might glean something from people such as Rob Bell or Donald Miller, but you can bet that we won't forget the <a href="http://vintage21.com/pages/link/video">Jesus Videos</a> as an honorable mention.  You'll also find the influences of Jack Hayford and his colleagues, who have been training a significant portion of our church that attends King's College and Seminary in Van Nuys, California.  
</p>

<p>Perhaps the best way to get to know us is <a href="sermons.php">hear what we've got to say</a>, but you're free to <a href="mailto:flcc@flcc.org">contact us</a> if you've got something you want to say to us.</p>

<?php
	require_once ('footer.php');
?>