<?php
	require_once ('header.php');

	if ($db = mysqli_connect($MYSQL_SERVER, $MYSQL_USER, $MYSQL_PASS, $MYSQL_DB))
	{
		// Get most recent year of sermons preached
		$q="SELECT substring(path,9,4) AS 'path' FROM sermon_index ORDER BY path DESC LIMIT 1;";
		$result=$db->query($q);
		$max_year=$result->fetch_assoc()['path'];

		// Get earliest year of sermons preached
		$q="SELECT substring(path,9,4) AS 'path' FROM sermon_index ORDER BY path ASC LIMIT 1;";
		$result=$db->query($q);
		$min_year=$result->fetch_assoc()['path'];

		//If we have correctly selected the database, get the stored info

		$year = isset($_GET['year']) ? $_GET['year'] : $max_year;

		switch($year) {
			case ($min_year <= $year && $year <= $max_year) :
				$year=mysqli_real_escape_string($db, stripslashes($year));
				break;
			case ($year=="all") :
				$year="200%";
				break;
			default :
				$year=$max_year;
				break;
		}
		
		$query = "SELECT `path`, `title`, `author`, `date_given`, `size` FROM `sermon_index` WHERE `hidden`=0 AND `date_given` LIKE '%$year' ORDER BY `path` DESC";
		$sermons = $db->query($query);
		$db->close();
	}
?>


	<header>
		<ul id="sermon_list">
		<?php foreach(range($min_year, $max_year) as $currentYear): ?>
			<li><a href="?year=<?=$currentYear?>"><?=$currentYear?></a></li>
		<?php endforeach; ?>
			<li><a href="?year=all">All</a></li>
		</ul>
		</header>
		<table cellspacing="0" id="sermons">
			<caption>Sermon archive (by year)</caption>
			<colgroup>
			<col width="40%">
			<col width="25%">
			<col width="25%">
			<col width="10%">
			</colgroup>
			<thead>
				<tr>
					<th scope="col" style="display:none"></th>
					<th scope="col">Title</th>
					<th scope="col">Author</th>
					<th scope="col">Date</th>
					<th scope="col">Size</th>
				</tr>
			</thead>
			<tbody>
				<?php while($row = $sermons->fetch_assoc()): ?>
					<tr>
						<td style="display:none;"><?=$row["path"]?></td>
						<td><a href="<?=$row["path"]?>"><?=$row["title"]?></a></td>
						<td><?=$row["author"]?></td>
						<td><?=$row["date_given"]?></td>
						<td><?=$row["size"]?> MB</td>
					</tr>
				<?php endwhile; ?>
			</tbody>
		</table>

<script type="text/javascript" charset="utf-8">



	$(document).ready(function() {
		$('#sermons').dataTable( {
			"order": [[0, 'desc']],
			"bPaginate": false,
			"bLengthChange": false,
			"bFilter": true,
			"bInfo": false,
			"bAutoWidth": false
		});
	});
</script>

<?php
	require_once('footer.php');
?>